package az.ingress.streams.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaTopicConfig {

    @Bean
    public NewTopic inTopic() {
        return new NewTopic("input-func-topic", 1, (short) 1);
    }

    @Bean
    public NewTopic outTopic() {
        return new NewTopic("output-func-topic", 1, (short) 1);
    }
}
