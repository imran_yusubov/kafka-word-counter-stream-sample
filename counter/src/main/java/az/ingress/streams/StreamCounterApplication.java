package az.ingress.streams;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class StreamCounterApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(StreamCounterApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Started printing the stream processing results....");
    }
}
